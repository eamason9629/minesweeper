import React from 'react';
import ReactDOM from 'react-dom';
import MinesweeperController from './controllers/MinesweeperController';

ReactDOM.render(
    <MinesweeperController />, document.getElementById('root')
);
