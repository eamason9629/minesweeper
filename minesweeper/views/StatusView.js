import React, { Component } from 'react';

class StatusView extends Component {
    render() {
        return <div>
            <div className='buttonRow'>
               <button onClick={this.props.onNewGame}>New Game</button>
               <select value={this.props.size} onChange={this.props.onChangeSize}>
                   <option value='small'>Small</option>
                   <option value='medium'>Medium</option>
                   <option value='large'>Large</option>
                   <option value='ludacris'>Ludacris</option>
               </select>
               <select value={this.props.difficulty} onChange={this.props.onChangeDifficulty}>
                   <option value='easiest'>Easiest</option>
                   <option value='easy'>Easy</option>
                   <option value='intermediate'>Intermediate</option>
                   <option value='hard'>Hard</option>
                   <option value='impossible'>Impossible</option>
               </select>
               <input type='checkbox' name='xray' value={this.props.xray} onChange={this.props.onChangeXRay} />X-ray
            </div>
            <div className='statusRow'>
               Moves: {this.props.moves}
               &nbsp;Mines left: {this.props.mineCounter}
           </div>
       </div>
    }
}

export default StatusView;
