import React, { Component } from 'react';
import SquareView from './SquareView';

class RowView extends Component {
    render() {
        return (
            <div className='mineRow'>
                {this.props.row.map((square, yIndex) => {
                    return <SquareView key={`${this.props.xindex}${yIndex}`} {...square} />
                })}
            </div>
        )
    }
}

export default RowView;
