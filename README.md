# Minesweeper clone

This is a simple Minesweeper clone created to test out the capabilities of React and Electron.

You can start this by typing 'npm install && node compileSass.js && npm start' into a terminal window.
